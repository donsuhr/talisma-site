const argv = require('yargs').argv;
const loadDotEnv = require('cmc-load-dot-env');
const algoliasearch = require('algoliasearch');

require('babel-register')({
    babelrc: false,
    only: [/build-algolia-index/, '/app/**/*.js'],
    presets: [
        [
            'env',
            {
                targets: {
                    node: 'current',
                },
            },
        ],
    ],
    plugins: [
        [
            'transform-object-rest-spread',
            {
                useBuiltIns: true,
            },
        ],
    ],
});
const buildSearchIndex = require('build-algolia-index').buildSearchIndex;

if (!{}.hasOwnProperty.call(process.env, 'CORS_ORIGIN_TLD')) {
    // eslint-disable-next-line no-console
    console.log('no CORS_ORIGIN_TLD set, will load .env');
    loadDotEnv();
}

const searchClient = algoliasearch(
    process.env.ALGOLIA_APPID,
    process.env.ALGOLIA_ADMIN_KEY
);

const dryRun = !!argv['dry-run'];

const groups = [
    {
        json: 'search-index--talisma--en-in.json',
        algoliaIndex: 'talisma--en-in',
        src: ['app/pages/**/*.html', '!app/pages/dev/**/*.html', '!app/pages/blog/**/*.html'],
    },
];

buildSearchIndex(
    groups,
    searchClient,
    dryRun,
    process.env.CORS_ORIGIN_CMC,
    process.env.CORS_ORIGIN_API
);
