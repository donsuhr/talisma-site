const path = require('path');
const fs = require('fs');
const crypto = require('crypto');
const exec = require('child_process').exec;
const spawn = require('child_process').spawn;

const hashFile = path.resolve(process.cwd(), 'yarnhash.json');

let hashFileJson;
try {
    hashFileJson = JSON.parse(fs.readFileSync(hashFile));
} catch (e) {
    hashFileJson = {};
}

const yarnFile = path.resolve(process.cwd(), 'yarn.lock');
const fileData = fs.readFileSync(yarnFile);

const currentHash = crypto.createHash('md5').update(fileData).digest('hex');

if (currentHash !== hashFileJson.hash) {
    const yarnInstall = spawn(
        /^win/.test(process.platform) ? 'yarn.cmd' : 'yarn',
        ['install'],
        { stdio: 'inherit' }
    );
    yarnInstall.on('exit', () => {
        // process.exit();
        hashFileJson.hash = currentHash;
        fs.writeFileSync(hashFile, JSON.stringify(hashFileJson, null, 4));
        const fixWinGruntFile = path.resolve(__dirname, 'fix-win-grunt.js');
        const fixWinGruntExec = exec(
            `node ${fixWinGruntFile}`,
            (err, stdout, stderr) => {
                if (err) {
                    // eslint-disable-next-line no-console
                    console.log('Error updating windows grunt file');
                    throw err;
                }
            }
        );
        fixWinGruntExec.stdout.pipe(process.stdout);
    });
} else {
    // eslint-disable-next-line no-console
    console.log('no yarn update');
}
