const path = require('path');
const fs = require('fs');

const isWin = /^win/.test(process.platform);
if (isWin) {
    // eslint-disable-next-line no-console
    console.log('copying windows grunt file');
    fs
        .createReadStream(path.resolve(__dirname, './win-grunt-cmds/grunt'))
        .pipe(
            fs.createWriteStream(
                path.resolve(__dirname, '../node_modules/.bin/grunt')
            )
        );
    fs
        .createReadStream(path.resolve(__dirname, './win-grunt-cmds/grunt.cmd'))
        .pipe(
            fs.createWriteStream(
                path.resolve(__dirname, '../node_modules/.bin/grunt.cmd')
            )
        );
}
