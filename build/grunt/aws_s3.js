module.exports = function awsS3(grunt, options) {
    return {
        'search-index-docs--get-general': {
            options: {
                bucket: 'cmcv3-general--talisma',
                differential: true,
            },
            files: [
                {
                    dest: '/',
                    cwd: 'search-index/cmcv3-general--talisma/',
                    action: 'download',
                },
            ],
        },
        'search-index-docs--get-resource-library': {
            options: {
                bucket: 'cmcv3-product-sheets--talisma--en-in',
                differential: true,
            },
            files: [
                {
                    dest: '/',
                    cwd: 'search-index/cmcv3-product-sheets--talisma--en-in/',
                    action: 'download',
                },
            ],
        },
    };
};
