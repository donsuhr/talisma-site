#!/usr/bin/env bash
yarn remove cmc-site cmc-write-config cmc-load-dot-env metalsmith-add-orig-file-name metalsmith-register-partials

yarn add \
git+ssh://git@bitbucket.org/zorg128/cmc-load-dot-env.git \
git+ssh://git@bitbucket.org/zorg128/cmc-site.git#static-site \
git+ssh://git@bitbucket.org/zorg128/cmc-write-config.git \
git+ssh://git@bitbucket.org/zorg128/metalsmith-add-orig-file-name.git \
git+ssh://git@bitbucket.org/zorg128/metalsmith-register-partials.git
