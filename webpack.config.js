// eslint-disable-next-line max-len
/* eslint comma-dangle: ["error", {"arrays": "always-multiline", "objects": "always-multiline", "imports": "always-multiline", "exports": "always-multiline", "functions": "never"}] */

const webpack = require('webpack');
const path = require('path');
const fs = require('fs');
const AssetsPlugin = require('assets-webpack-plugin');

const production = process.env.NODE_ENV === 'production';
const entry = {
    'cmc-site': [path.join(__dirname, 'app/scripts/index.js')],
    'page--home': [
        path.join(__dirname, 'node_modules/cmc-site/app/scripts/pages/home'),
    ],
    'page--s3-doc-list': [
        path.join(
            __dirname,
            'node_modules/cmc-site/app/scripts/pages/s3-doc-list'
        ),
    ],
    'page--partners': [
        path.join(
            __dirname,
            'node_modules/cmc-site/app/scripts/pages/partners'
        ),
    ],
    'page--jwplayer': [
        path.join(
            __dirname,
            'node_modules/cmc-site/app/scripts/pages/jwplayer'
        ),
    ],
    'page--press-releases': [
        path.join(
            __dirname,
            'node_modules/cmc-site/app/scripts/pages/press-releases'
        ),
    ],
    'page--contact-us': [
        path.join(
            __dirname,
            'node_modules/cmc-site/app/scripts/pages/contact-us'
        ),
    ],
    'page--contact-complete': [
        path.join(
            __dirname,
            'app/scripts/pages/contact-complete'
        ),
    ],
    'page--product-center-cookie-check': [
        path.join(
            __dirname,
            'node_modules/cmc-site/app/scripts/pages/product-center-cookie-check'
        ),
    ],
    'page--product-center': [
        path.join(
            __dirname,
            'node_modules/cmc-site/app/scripts/pages/product-center'
        ),
    ],
    'page--product-center-register': [
        path.join(
            __dirname,
            'node_modules/cmc-site/app/scripts/pages/product-center-register'
        ),
    ],
    'page--404': [
        path.join(
            __dirname,
            'node_modules/cmc-site/app/scripts/pages/404'
        ),
    ],
    'page--search': [
        path.join(
            __dirname,
            'node_modules/cmc-site/app/scripts/pages/search'
        ),
    ],
    'page--simple-form': [
        path.join(
            __dirname,
            'node_modules/cmc-site/app/scripts/pages/simple-form'
        ),
    ],
    'page--password-protected-download': [
        path.join(
            __dirname,
            'node_modules/cmc-site/app/scripts/pages/password-protected-download'
        ),
    ],
    'page--simple-form--with-formalizer': [
        path.join(
            __dirname,
            'node_modules/cmc-site/app/scripts/pages/simple-form--with-formalizer'
        ),
    ],
};

const plugins = [
    new AssetsPlugin(),
    new webpack.NoEmitOnErrorsPlugin(),
    new webpack.ProvidePlugin({
        'window.jQuery': 'jquery', // garlic, headroom, bootstrap
        jQuery: 'jquery', //  bootstrap parsley superfish
    }),
    new webpack.DefinePlugin({
        'process.env': {
            NODE_ENV: JSON.stringify(process.env.NODE_ENV || 'development'),
            NETLIFY_ENV: JSON.stringify(
                process.env.NETLIFY_ENV || 'development'
            ),
        },
    }),
    new webpack.optimize.CommonsChunkPlugin({
        name: 'commons--bsapps',
        minChunks: 2,
        chunks: [
            'page--partners',
            'page--s3-doc-list',
            'page--press-releases',
            'page--password-protected-download',
        ],
    }),
    new webpack.optimize.CommonsChunkPlugin({
        name: 'commons--forms',
        chunks: [
            'page--contact-us',
            'page--simple-form',
            'page--simple-form--with-formaizer',
        ],
    }),
    new webpack.optimize.CommonsChunkPlugin({
        name: 'cmc-site',
        minChunks: 5,
    }),
];
if (production) {
    plugins.push(
        new webpack.optimize.UglifyJsPlugin({
            mangle: true,
            sourceMap: true,
            output: {
                comments: false,
            },
            compress: {
                warnings: false,
                drop_debugger: false,
            },
        })
    );
} else {
    entry['cmc-site'].unshift(
        'webpack/hot/dev-server',
        'webpack-hot-middleware/client'
    );
    plugins.push(new webpack.HotModuleReplacementPlugin());
    const manifestFile = path.resolve(process.cwd(), 'dll-manifest.json');
    let manifestJson;
    try {
        manifestJson = JSON.parse(fs.readFileSync(manifestFile));
    } catch (e) {
        manifestJson = false;
    }
    if (manifestJson) {
        plugins.push(
            new webpack.DllReferencePlugin({
                context: '.',
                manifest: manifestJson,
            })
        );
    }
}

module.exports = {
    stats: 'errors-only',
    devtool: production ? 'source-map' : '#eval-source-map',
    context: path.join(__dirname, 'app', 'scripts'),
    entry,
    plugins,
    resolve: {
        modules: ['node_modules', path.resolve(__dirname, 'app')],
        alias: {
            config: path.join(__dirname, 'config.js'),
            underscore: path.join(
                __dirname,
                'node_modules/underscore/underscore'
            ),
            handlebars: 'handlebars/runtime',
        },
    },
    output: {
        path: path.join(__dirname, 'dist/'),
        filename: production ? 'scripts/[name]-[chunkhash].js' : 'scripts/[name].js',
        chunkFilename: production
            ? 'scripts/[name]-[chunkhash].js'
            : 'scripts/[name].js',
        publicPath: '/',
    },
    module: {
        rules: [
            {
                test: /nls[/\\]\S*\.json/,
                use: [
                    {
                        loader: 'amdi18n-loader',
                    },
                ],
                include: [
                    path.join(__dirname, '/app/components/'),
                    path.join(__dirname, '/node_modules/cmc-site/app/'),
                ],
            },
            {
                test: /\.js$/,
                use: [
                    {
                        loader: 'babel-loader',
                    },
                ],
                include: [
                    path.join(__dirname, '/app/'),
                    path.join(__dirname, '/config.js'),
                    path.join(__dirname, '/node_modules/cmc-site'),
                    path.join(__dirname, '/node_modules/autotrack'),
                    path.join(__dirname, '/node_modules/dom-utils'),
                ],
            },
            {
                test: /\.hbs$/,
                use: [
                    {
                        loader: 'handlebars-loader',
                        query: {
                            debug: false,
                            runtime: 'handlebars/runtime',
                            helperDirs: [
                                `${__dirname}/app/metalsmith/helpers`,
                                `${__dirname}/node_modules/cmc-site/app/metalsmith/helpers`,
                            ],
                            partialDirs: [
                                `${__dirname}/app/metalsmith/partials`,
                                `${__dirname}/node_modules/cmc-site/app/metalsmith/partials`,
                            ],
                        },
                    },
                ],
                include: [
                    path.join(__dirname, '/app/'),
                    path.join(__dirname, '/node_modules/cmc-site'),
                ],
            },
            {
                test: /\.html$/,
                use: [
                    {
                        loader: 'underscore-template-loader',
                        query: {
                            variable: 'data',
                        },
                    },
                ],
            },
        ],
    },
};
